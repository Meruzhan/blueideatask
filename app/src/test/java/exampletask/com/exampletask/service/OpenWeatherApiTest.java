package exampletask.com.exampletask.service;

import com.github.tomakehurst.wiremock.junit.WireMockRule;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import exampletask.com.exampletask.api.OpenWeatherApi;

import exampletask.com.exampletask.model.weatherModel.WeatherCity;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static org.fest.assertions.api.Assertions.assertThat;

public class OpenWeatherApiTest {



    private WeatherService weatherService;

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(8000);

    @Before
    public void setup() {
        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync()).
                        addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://localhost:8000").build();
        this.weatherService = new WeatherService(retrofit.create(OpenWeatherApi.class));
    }

    @Test
    public void testGetCurrentWeatherReturnsExpected() {
        stubFor(get(urlMatching("/data/2.5/.*"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withBodyFile("weather-api-search[London].json")));
        WeatherCity result = weatherService.getWeatherCity("London").blockingFirst();
        assertThat(result.getId()).isNotNull();
        assertThat(result.getName()).isNotNull();
        assertThat(result.getMain().getTemp()).isNotNull();
        assertThat(result.getWind()).isNotNull();
        assertThat(result.getWind().getSpeed()).isNotNull();
        assertThat(result.getWind().getDeg()).isNotNull();
    }
}
