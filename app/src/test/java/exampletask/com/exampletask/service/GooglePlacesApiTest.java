package exampletask.com.exampletask.service;

import com.github.tomakehurst.wiremock.junit.WireMockRule;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;


import exampletask.com.exampletask.api.GooglePlacesApi;
import exampletask.com.exampletask.model.cityModels.CitiesData;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.fest.assertions.api.Assertions.assertThat;

public class GooglePlacesApiTest {



    private CityCatalogService cityCatalogService;

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(8000);

    @Before
    public void setup() {
        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync()).
                        addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://localhost:8000").build();
        this.cityCatalogService = new CityCatalogService(retrofit.create(GooglePlacesApi.class));
    }

    @Test
    public void testSearchCityReturnsExpected() {
        stubFor(get(urlMatching("/maps/api/.*"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withBodyFile("google-api-search[Yer].json")));
        CitiesData result = cityCatalogService.searchCity("Yer").blockingFirst();
        assertThat(result.getPredictions()).hasSize(5);
        assertThat(result.getPredictions().get(0).getDescription()).isEqualTo("Yerevan, Armenia");
        assertThat(result.getPredictions().get(0).getId()).isEqualTo("c4bb792b33ea114c43c458957c46f3ced86ea8f9");
    }
}
