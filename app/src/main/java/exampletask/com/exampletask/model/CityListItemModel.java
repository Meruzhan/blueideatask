package exampletask.com.exampletask.model;

/**
 * Created by Gasparyan on 14.07.2018.
 */

public class CityListItemModel {
    private String cityName;
    private Double tepm;
    public CityListItemModel(String cityName,Double temp) {
        this.cityName = cityName;
        this.tepm = temp;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Double getTepm() {
        return tepm;
    }

    public void setTepm(Double tepm) {
        this.tepm = tepm;
    }
}
