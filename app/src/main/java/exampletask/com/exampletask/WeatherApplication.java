package exampletask.com.exampletask;

import android.app.Application;

import exampletask.com.exampletask.di.component.DaggerMainComponent;
import exampletask.com.exampletask.di.component.MainComponent;
import exampletask.com.exampletask.di.modules.ApiModule;


public class WeatherApplication extends Application {

    private static WeatherApplication instance;

    MainComponent component;

    public static WeatherApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        component = DaggerMainComponent
                .builder()
                .apiModule(new ApiModule())
                .build();
    }

    public MainComponent getComponent() {
        return component;
    }
}