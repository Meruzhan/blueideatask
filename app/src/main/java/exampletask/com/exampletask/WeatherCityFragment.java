package exampletask.com.exampletask;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import javax.inject.Inject;

import exampletask.com.exampletask.service.WeatherService;
import exampletask.com.exampletask.viewmodel.CitiesListViewModel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * @author meruzhan.gasparyan on 14-Jul-18.
 */

public class WeatherCityFragment extends Fragment {

    @Inject
    WeatherService weatherService;

    @Inject
    CitiesListViewModel viewModel;

    private Disposable subscribe;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WeatherApplication.getInstance().getComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.weather_city_data, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView cityNameview = (TextView) view.findViewById(R.id.cityName);
        TextView temp = (TextView) view.findViewById(R.id.cityTemp);
        TextView description = (TextView) view.findViewById(R.id.description);
        TextView main = (TextView) view.findViewById(R.id.main);
        TextView tempMin = (TextView) view.findViewById(R.id.temp_min);
        TextView tempMax = (TextView) view.findViewById(R.id.temp_max);
        String cityName = this.viewModel.getSelectedCity();
        subscribe = weatherService.getWeatherCity(cityName).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()).subscribe((data) -> {
                    cityNameview.setText(cityName );
                    temp.setText("Temp = "+data.getMain().getTemp());
                    description.setText("Description = "+data.getWeather().get(0).getDescription());
                    main.setText("Main = "+data.getWeather().get(0).getMain());
                    tempMax.setText("Max Temp = "+data.getMain().getTempMax());
                    tempMin.setText("Max Temp = "+data.getMain().getTempMin());
                });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.subscribe.dispose();
    }
}
