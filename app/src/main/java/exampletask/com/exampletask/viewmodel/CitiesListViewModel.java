package exampletask.com.exampletask.viewmodel;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.ViewModel;

import java.io.Serializable;

import javax.inject.Inject;
import javax.inject.Singleton;

import exampletask.com.exampletask.model.cityModels.CitiesData;
import exampletask.com.exampletask.service.CityCatalogService;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * @author meruzhan.gasparyan on 14-Jul-18.
 */
@Singleton
public class CitiesListViewModel extends ViewModel implements Serializable{

    static final long serialVersionUID = -687991492884005033L;

    private final MediatorLiveData<CitiesData> citiesData;
    private final CityCatalogService cityCatalogService;

    private final CompositeDisposable disposables = new CompositeDisposable();
    private String selectedCity;

    @Inject
    public CitiesListViewModel(CityCatalogService cityCatalogService) {
        this.cityCatalogService = cityCatalogService;
        citiesData = new MediatorLiveData<>();
        citiesData.setValue(null);

    }

    public void searchCities(String term) {
        disposables.add(this.cityCatalogService.searchCity(term).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(citiesData::setValue));
    }

    public LiveData<CitiesData> getCities() {
        return citiesData;
    }

    public void selectCity(String citiesData) {
        this.selectedCity = citiesData;
    }

    public String getSelectedCity() {
        return selectedCity;
    }
}
