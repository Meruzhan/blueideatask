package exampletask.com.exampletask.api;


import exampletask.com.exampletask.model.weatherModel.WeatherCity;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by meruzhan.gasparyan on 14-Jul-18.
 * API to get weather by city name
 */

public interface OpenWeatherApi {

    @GET("data/2.5/weather?appid=b6907d289e10d714a6e88b30761fae22")//4ecfe66eb1198cd8eef4688cb1a0d209
    Observable<WeatherCity> getWeatherCity(@Query("q") String cityName);

}
