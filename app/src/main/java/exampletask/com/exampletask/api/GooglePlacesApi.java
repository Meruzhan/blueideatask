package exampletask.com.exampletask.api;

import exampletask.com.exampletask.model.cityModels.CitiesData;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by meruzhan.gasparyan on 14-Jul-18.
 * Google API to search cities.
 */

public interface GooglePlacesApi {
    @GET("maps/api/place/autocomplete/json?&key=AIzaSyCELsQdKgklBSo7P1Z4TBAYLeRcKmPW4wU")
    Observable<CitiesData> searchCity(@Query("input")String term);
}
