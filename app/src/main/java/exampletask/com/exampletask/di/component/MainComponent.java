package exampletask.com.exampletask.di.component;

import javax.inject.Singleton;

import dagger.Component;
import exampletask.com.exampletask.CityListFragment;
import exampletask.com.exampletask.WeatherCityFragment;
import exampletask.com.exampletask.di.modules.ApiModule;


@Singleton
@Component(modules = {ApiModule.class})
/**
 * Dagger component for DI
 */
public interface MainComponent {

    void inject(CityListFragment cityListFragment);

    void inject(WeatherCityFragment weatherCityFragment);
}