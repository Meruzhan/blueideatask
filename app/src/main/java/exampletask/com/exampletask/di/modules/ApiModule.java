package exampletask.com.exampletask.di.modules;

import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import exampletask.com.exampletask.api.GooglePlacesApi;
import exampletask.com.exampletask.api.OpenWeatherApi;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApiModule {

    private static final String CITY_SERVICE_URL = "https://maps.googleapis.com";
    private static final String WEATHER_SERVICE_URL = "https://samples.openweathermap.org";// real Api://api.openweathermap.org

    @Provides
    @Singleton
    public GooglePlacesApi provideGooglePlacesApi(OkHttpClient okHttpClient,
                                           GsonConverterFactory gsonConverterFactory) {
        Retrofit retrofit = new Retrofit.Builder()
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync()).
                        addConverterFactory(gsonConverterFactory)
                .baseUrl(CITY_SERVICE_URL).build();

        return retrofit.create(GooglePlacesApi.class);
    }

    @Provides
    @Singleton
    public OpenWeatherApi provideOpenWeatherApi(OkHttpClient okHttpClient,
                                         GsonConverterFactory gsonConverterFactory) {
        Retrofit retrofit = new Retrofit.Builder()
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync()).
                        addConverterFactory(gsonConverterFactory)
                .baseUrl(WEATHER_SERVICE_URL).build();
        return retrofit.create(OpenWeatherApi.class);
    }

    @Provides
    @Singleton
    public OkHttpClient provideHttpClient() {
        return new OkHttpClient().newBuilder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS).build();
    }

    @Provides
    @Singleton
    public GsonConverterFactory provideGsonConverterFactory() {
        return GsonConverterFactory.create(new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create());
    }
}
