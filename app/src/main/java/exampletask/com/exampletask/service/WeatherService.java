package exampletask.com.exampletask.service;

import javax.inject.Inject;
import javax.inject.Singleton;

import exampletask.com.exampletask.api.OpenWeatherApi;
import exampletask.com.exampletask.model.weatherModel.WeatherCity;
import io.reactivex.Observable;

/**
 * @author meruzhan.gasparyan on 12-Jul-18.
 * Provides weather information for requested city.
 */
@Singleton
public class WeatherService {

    private final OpenWeatherApi openWeatherApi;

    @Inject
    public WeatherService(OpenWeatherApi openWeatherApi) {
        this.openWeatherApi = openWeatherApi;
    }

    public Observable<WeatherCity> getWeatherCity(String cityName) {
        return this.openWeatherApi.getWeatherCity(cityName);
    }
}
