package exampletask.com.exampletask.service;

import javax.inject.Inject;
import javax.inject.Singleton;

import exampletask.com.exampletask.api.GooglePlacesApi;
import exampletask.com.exampletask.model.cityModels.CitiesData;
import io.reactivex.Observable;

/**
 * @author meruzhan.gasparyan on 14-Jul-18.
 * Service to loads  cities .
 */
@Singleton
public class CityCatalogService {

    private final GooglePlacesApi googlePlacesApi;

    @Inject
    public CityCatalogService(GooglePlacesApi googlePlacesApi) {
        this.googlePlacesApi = googlePlacesApi;
    }

    /**
     * Provide list of cities by search term
     */
    public Observable<CitiesData> searchCity(String searchTerm) {
        return googlePlacesApi.searchCity(searchTerm);

    }
}
