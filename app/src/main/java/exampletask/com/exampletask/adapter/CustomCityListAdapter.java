package exampletask.com.exampletask.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import exampletask.com.exampletask.R;
import exampletask.com.exampletask.model.CityListItemModel;

/**
 * @author meruzhan.gasparyan on 14-Jul-18.
 */

public class CustomCityListAdapter extends ArrayAdapter<CityListItemModel> {


    int resource;
    String response;
    Context context;
    public CustomCityListAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<CityListItemModel> objects) {
        super(context, resource, objects);
        this.resource=resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LinearLayout alertView;
      if(convertView !=null) {
          alertView = (LinearLayout) convertView;
      }else{
          alertView = new LinearLayout(getContext());
              String inflater = Context.LAYOUT_INFLATER_SERVICE;
              LayoutInflater vi;
              vi = (LayoutInflater)getContext().getSystemService(inflater);
              vi.inflate(resource, alertView, true);
      }
        TextView cityName = (TextView) alertView.findViewById(R.id.cityName);
        TextView temp = (TextView) alertView.findViewById(R.id.cityTemp);
        cityName.setText(getItem(position).getCityName());
        temp.setText(" "+getItem(position).getTepm()+" C");
        return alertView;
    }
}
