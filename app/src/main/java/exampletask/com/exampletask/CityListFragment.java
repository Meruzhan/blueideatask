package exampletask.com.exampletask;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LifecycleRegistry;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import exampletask.com.exampletask.adapter.CustomCityListAdapter;
import exampletask.com.exampletask.model.CityListItemModel;
import exampletask.com.exampletask.model.weatherModel.WeatherCity;
import exampletask.com.exampletask.service.CityCatalogService;
import exampletask.com.exampletask.service.WeatherService;
import exampletask.com.exampletask.viewmodel.CitiesListViewModel;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * @author meruzhan.gasparyan on 14-Jul-18.
 */

public class CityListFragment extends Fragment implements LifecycleOwner {


    @Inject
    WeatherService weatherService;

    @Inject
    CityCatalogService cityCatalogService;

    @Inject
    CitiesListViewModel viewModel;

    private ListView listView;
    private LifecycleRegistry mLifecycleRegistry;
    private WeatherCityFragment weatherCityFragment;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mLifecycleRegistry = new LifecycleRegistry(this);
        mLifecycleRegistry.markState(Lifecycle.State.CREATED);
        WeatherApplication.getInstance().getComponent().inject(this);

        this.weatherCityFragment = new WeatherCityFragment();
    }

    @Override
    public void onStart() {
        super.onStart();
        mLifecycleRegistry.markState(Lifecycle.State.STARTED);
    }

    public void searchCity(String term) {
        this.viewModel.searchCities(term);
    }

    public void selectCity(String id) {
        this.viewModel.selectCity(id);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.city_list_fragment, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EditText searchInput = (EditText) view.findViewById(R.id.searchInput);

        listView = (ListView) view.findViewById(R.id.cityList);
        viewModel.getCities().observe(this, cd -> {

            if (cd!=null){
                List<String> cityNames = cd.getPredictions().stream().map(p -> p.getDescription()).collect(Collectors.toList());
                List<Observable<WeatherCity>> cityTemps = cityNames.stream().map(data->weatherService.getWeatherCity(data)).collect(Collectors.toList());
                Observable.combineLatest(cityTemps,(watchCityDatas)->{
                    List<CityListItemModel> cityListItemModels = new ArrayList<CityListItemModel>();
                    for(String cityName:cityNames){
                        for (Object weatherCityData:watchCityDatas){
//                            if(cityName.equals(((WeatherCity)watchCityData).getName())){
                                cityListItemModels.add(new CityListItemModel(cityName,((WeatherCity)weatherCityData).getMain().getTemp()));
                                break;
//                            }
                        }
                    }
                return cityListItemModels;
                }).subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(cityListItemModels-> listView.setAdapter(new CustomCityListAdapter(view.getContext(), R.layout.list_view_item, cityListItemModels)) );


            }
        });


        searchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                searchCity(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        listView.setOnItemClickListener((parent, viewItem, position, id) -> {
                    TextView textView =(TextView) ((View) viewItem).findViewById(R.id.cityName);
                    viewModel.selectCity(textView.getText().toString());
                    goToCityPage();
                }
        );
    }

    void goToCityPage() {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, this.weatherCityFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        return mLifecycleRegistry;
    }
}
